package it.unibo.sa.respect.swarm.gui;

import javax.swing.*;
import java.awt.*;

/**
 * @author ste
 */
public final class GUI {

    /**
     *
     */
    public static void init() {
        /*
         * Schedule a job for the event-dispatching thread: creating and showing
         * this application's GUI.
         */
        SwingUtilities.invokeLater(GUI::createAndShowGUI);
    }

    private static void createAndShowGUI() {
        // Create and set up the window.
        final SwarmFrame frame = new SwarmFrame("Uniform Reading Ants");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
        SwingUtilities.invokeLater(() -> {
            frame.getContentPane().add(new SwarmComponent(), BorderLayout.CENTER);
            frame.revalidate();
            frame.repaint();
            frame.getContentPane().revalidate();
            frame.getContentPane().repaint();
        });
    }

}
