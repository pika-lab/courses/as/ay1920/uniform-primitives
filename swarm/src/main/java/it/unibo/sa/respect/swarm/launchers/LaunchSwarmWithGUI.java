package it.unibo.sa.respect.swarm.launchers;

import alice.tuplecentre.tucson.api.exceptions.TucsonInvalidAgentIdException;
import alice.tuplecentre.tucson.api.exceptions.TucsonInvalidTupleCentreIdException;
import it.unibo.sa.respect.swarm.ant.Swarm;
import it.unibo.sa.respect.swarm.gui.GUI;

/**
 * @author Stefano Mariani (mailto: s [dot] mariani [at] unibo [dot] it)
 */
public final class LaunchSwarmWithGUI {

    /**
     * @param args
     */
    public static void main(String[] args) throws TucsonInvalidAgentIdException, TucsonInvalidTupleCentreIdException {
        GUI.init();

        Swarm.release();
    }

}
