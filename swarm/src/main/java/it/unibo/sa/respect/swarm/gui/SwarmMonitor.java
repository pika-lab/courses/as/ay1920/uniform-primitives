package it.unibo.sa.respect.swarm.gui;

import alice.tuple.logic.LogicTuple;
import alice.tuple.logic.exceptions.InvalidLogicTupleException;
import alice.tuplecentre.api.exceptions.OperationTimeOutException;
import alice.tuplecentre.tucson.api.TucsonAgentId;
import alice.tuplecentre.tucson.api.TucsonMetaACC;
import alice.tuplecentre.tucson.api.TucsonOperation;
import alice.tuplecentre.tucson.api.TucsonTupleCentreId;
import alice.tuplecentre.tucson.api.acc.BulkSyncACC;
import alice.tuplecentre.tucson.api.acc.NegotiationACC;
import alice.tuplecentre.tucson.api.exceptions.TucsonInvalidAgentIdException;
import alice.tuplecentre.tucson.api.exceptions.TucsonInvalidTupleCentreIdException;
import alice.tuplecentre.tucson.api.exceptions.TucsonOperationNotPossibleException;
import alice.tuplecentre.tucson.api.exceptions.UnreachableNodeException;

import javax.swing.*;
import java.awt.*;
import java.util.List;

/**
 * @author ste
 */
public class SwarmMonitor {

    final SwarmComponent component;
    private final JButton[] bs;
    private final TucsonTupleCentreId[] tcids;
    private BulkSyncACC acc;

    /**
     * @param c
     */
    public SwarmMonitor(final SwarmComponent c) throws OperationTimeOutException, TucsonInvalidAgentIdException, UnreachableNodeException, TucsonOperationNotPossibleException, TucsonInvalidTupleCentreIdException {

        this.component = c;
        final Component[] cs = this.component.getComponents();
        this.bs = new JButton[cs.length - 3];
        this.tcids = new TucsonTupleCentreId[cs.length - 3];
        for (int i = 1; i < (cs.length - 2); i++) {
            this.bs[i - 1] = (JButton) cs[i];
            this.tcids[i - 1] = TucsonTupleCentreId.of(this.bs[i - 1].getName(), "localhost", "" + (20504 + i));
            SwarmMonitor.log("" + this.bs[i - 1].getName());
            SwarmMonitor.log("" + this.tcids[i - 1].getLocalName() + ":" + this.tcids[i - 1].getPort());
        }
        NegotiationACC negAcc = TucsonMetaACC.getNegotiationContext(TucsonAgentId.of("tcsMonitor"));
        this.acc = negAcc.playDefaultRole();
    }

    private static void log(final String msg) {
        System.out.println("[TcsMonitor]: " + msg);
    }

    private static void err(final String msg) {
        System.err.println(msg);
    }

    /**
     *
     */
    public void performUpdate() throws OperationTimeOutException, InvalidLogicTupleException, TucsonOperationNotPossibleException, UnreachableNodeException {
        // new Thread(() -> {
        final int[] pheromones = smell();
        for (int i = 0; i < bs.length; i++) {
            final int j = i;
            SwingUtilities.invokeLater(() -> {
                bs[j].setText("[TC-" + bs[j].getName() + "] : " + pheromones[j]);
                component.revalidate();
            });

        }
        // SwingUtilities.invokeLater(() -> frame
        // .repaint());
        SwingUtilities.invokeLater(() -> {
            component.getParent().revalidate();
            component.getParent().repaint();
            component.revalidate();
            component.repaint();
        });
        // }).start();
    }

    private int[] smell() throws InvalidLogicTupleException, UnreachableNodeException, OperationTimeOutException, TucsonOperationNotPossibleException {
        final int[] pheromones = new int[this.tcids.length];
        TucsonOperation op;
        List<LogicTuple> tuples;
        for (int i = 0; i < this.tcids.length; i++) {
            SwarmMonitor.log("Smelling " + this.tcids[i].getLocalName() + "...");
            op = this.acc.rdAll(this.tcids[i], LogicTuple.parse("nbr(N)"), null);
            if (op.isResultSuccess()) {
                tuples = op.getLogicTupleListResult();
                pheromones[i] = tuples.size();
                SwarmMonitor.log("..." + pheromones[i] + " pheromones found!");
            } else {
                pheromones[i] = -1;
                SwarmMonitor.err("Error while smelling for update: <rd_all> failure!");
            }
        }
        return pheromones;
    }

}
