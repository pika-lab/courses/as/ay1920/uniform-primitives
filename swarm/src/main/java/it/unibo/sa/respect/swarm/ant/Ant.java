/**
 *
 */
package it.unibo.sa.respect.swarm.ant;

import alice.tuple.logic.LogicTuple;
import alice.tuple.logic.exceptions.InvalidLogicTupleException;
import alice.tuplecentre.api.exceptions.OperationTimeOutException;
import alice.tuplecentre.tucson.api.*;
import alice.tuplecentre.tucson.api.acc.EnhancedSyncACC;
import alice.tuplecentre.tucson.api.exceptions.TucsonInvalidAgentIdException;
import alice.tuplecentre.tucson.api.exceptions.TucsonInvalidTupleCentreIdException;
import alice.tuplecentre.tucson.api.exceptions.TucsonOperationNotPossibleException;
import alice.tuplecentre.tucson.api.exceptions.UnreachableNodeException;

/**
 * @author ste
 *
 */
public class Ant extends AbstractTucsonAgent<EnhancedSyncACC> {

    private final static Long TIMEOUT = 500L;

    private TucsonTupleCentreId tcid;
    private EnhancedSyncACC acc;

    private boolean stopped;
    private boolean carryingFood;

    /**
     * @param aid
     * @param netid
     * @param port
     * @param tcName
     * @throws TucsonInvalidAgentIdException
     */
    public Ant(String aid, String netid, int port, String tcName) throws TucsonInvalidAgentIdException, TucsonInvalidTupleCentreIdException {
        super(aid, netid, port);
        acc = null;
        tcid = TucsonTupleCentreId.of(tcName, netid, "" + port);
        stopped = false;
        carryingFood = false;
    }

    @Override
    protected EnhancedSyncACC retrieveACC(TucsonAgentId tucsonAgentId, String s, int i) throws Exception {
        return TucsonMetaACC.getNegotiationContext(getTucsonAgentId()).playDefaultRole();
    }

    /*
     * (non-Javadoc)
     *
     * @see alice.tucson.api.TucsonAgent#main()
     */
    @Override
    protected void main() throws Exception {

        acc = getACC();

        init();

        say("Hello!");

        boolean isFood = false;
        LogicTuple direction = null;

        while (!stopped) {
            if (!carryingFood) {
                isFood = smellFood();
                if (isFood) {
                    pickFood();
                } else {
                    say("No food here :(");
                    direction = smellPheromone();
                    move(direction);
                    say("Wandering toward %s", direction);
                }
            } else {
                if (isAnthill()) {
                    dropFood();
                } else {
                    direction = smellAnthill();
                    move(direction);
                    say("Bringing home food...");
                }
            }

            try {
                Thread.sleep(TIMEOUT);
            } catch (final InterruptedException e) {
                stopped = true;
            }

        }

        say("Bye bye!");

    }

    /**
     *
     */
    private void init() throws InvalidLogicTupleException, UnreachableNodeException, OperationTimeOutException, TucsonOperationNotPossibleException {
        acc.out(tcid, LogicTuple.parse("ant(" + getTucsonAgentId().getLocalName() + ")"), null);
    }

    private boolean smellFood() throws InvalidLogicTupleException, UnreachableNodeException, OperationTimeOutException, TucsonOperationNotPossibleException {
        say("Smelling food...");
        TucsonOperation op = acc.urdp(tcid, LogicTuple.parse("food"), TIMEOUT);
        return op.isResultSuccess();
    }

    private void pickFood() throws InvalidLogicTupleException, UnreachableNodeException, OperationTimeOutException, TucsonOperationNotPossibleException {
        TucsonOperation op = acc.uinp(tcid, LogicTuple.parse("food"), TIMEOUT);
        carryingFood = op.isResultSuccess();
        say("Food found :)");
    }

    private LogicTuple smellPheromone() throws InvalidLogicTupleException, UnreachableNodeException, OperationTimeOutException, TucsonOperationNotPossibleException {
        TucsonOperation op = acc.urdp(tcid, LogicTuple.parse("nbr(NBR)"), TIMEOUT);
        if (op.isResultSuccess()) {
            return op.getLogicTupleResult();
        }
        err("Error while smelling pheromone: no nbrs found!");
        return null;
    }

    private void move(final LogicTuple direction) throws TucsonInvalidTupleCentreIdException, InvalidLogicTupleException, UnreachableNodeException, OperationTimeOutException, TucsonOperationNotPossibleException {

        TucsonOperation op = acc.uinp(tcid, LogicTuple.parse("ant(" + getTucsonAgentId().getLocalName() + ")"), TIMEOUT);

        if (op.isResultSuccess()) {

            TucsonTupleCentreId oldTcid = null;

            if (carryingFood) {
                oldTcid = TucsonTupleCentreId.of(tcid.getLocalName(), tcid.getNode(), "" + tcid.getPort());
            }
            tcid = TucsonTupleCentreId.of(
                    direction.getArg(0).getArg(0).toString(),
                    direction.getArg(0).getArg(1).getArg(0).toString(),
                    direction.getArg(0).getArg(1).getArg(1).toString()
            );

            acc.out(tcid, LogicTuple.parse("ant(" + getTucsonAgentId().getLocalName() + ")"), TIMEOUT);
            if (carryingFood) {
                say("Leaving pheromone...");
                acc.out(tcid, LogicTuple.parse("nbr(" + oldTcid + ")"), TIMEOUT);
            }

        } else {
            err("Error while moving: cannot find myself!");
        }

    }

    private boolean isAnthill() {
        return ("anthill".equals(tcid.getLocalName()));
    }

    private void dropFood() throws InvalidLogicTupleException, UnreachableNodeException, OperationTimeOutException, TucsonOperationNotPossibleException {

        TucsonOperation op = acc.out(tcid, LogicTuple.parse("stored_food"), TIMEOUT);

        carryingFood = !op.isResultSuccess();
        say("Job done!");
    }

    private LogicTuple smellAnthill() throws InvalidLogicTupleException, UnreachableNodeException, OperationTimeOutException, TucsonOperationNotPossibleException {

        TucsonOperation op = acc.urdp(tcid, LogicTuple.parse("anthill(NEXT)"), TIMEOUT);
        if (op.isResultSuccess()) {
            return op.getLogicTupleResult();
        }
        err("Error while smelling anthill: no anthill found!");
        return null;
    }

    private void err(final String msg) {
        System.err.println("[" + getTucsonAgentId().getLocalName() + ": " + msg);
    }

}
