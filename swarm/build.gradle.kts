task<JavaExec>("runSwarm") {
    dependsOn("classes")
//    group = "exercise2"
    sourceSets {
        main {
            classpath = runtimeClasspath
        }
    }
    main = "it.unibo.sa.respect.swarm.launchers.LaunchSwarmsScenario"
    standardInput = System.`in`

}

task<JavaExec>("buildAntsTopology") {
    dependsOn("classes")
//    group = "exercise2"
    sourceSets {
        main {
            classpath = runtimeClasspath
        }
    }
    main = "it.unibo.sa.respect.swarm.launchers.LaunchTopology"
    standardInput = System.`in`

}

task<JavaExec>("buildAntsEnvironment") {
    dependsOn("classes")
//    group = "exercise2"
    sourceSets {
        main {
            classpath = runtimeClasspath
        }
    }
    main = "it.unibo.sa.respect.swarm.launchers.LaunchEnvironment"
    standardInput = System.`in`

}

task<JavaExec>("runGUI") {
    dependsOn("classes")
//    group = "exercise2"
    sourceSets {
        main {
            classpath = runtimeClasspath
        }
    }
    main = "it.unibo.sa.respect.swarm.launchers.LaunchSwarmWithGUI"
    standardInput = System.`in`

}