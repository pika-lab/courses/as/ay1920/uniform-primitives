package it.unibo.sa.respect.loadb;

import alice.tuple.logic.LogicTuple;
import alice.tuplecentre.tucson.api.*;
import alice.tuplecentre.tucson.api.acc.EnhancedSyncACC;
import alice.tuplecentre.tucson.api.exceptions.TucsonInvalidAgentIdException;

public class Stopper extends AbstractTucsonAgent<EnhancedSyncACC> {

    public static void main(final String[] args) throws TucsonInvalidAgentIdException {
        new Stopper("stopper").go();
    }

    public Stopper(final String aid) throws TucsonInvalidAgentIdException {
        super(aid);
    }

    @Override
    protected EnhancedSyncACC retrieveACC(TucsonAgentId tucsonAgentId, String s, int i) throws Exception {
        return TucsonMetaACC.getNegotiationContext(getTucsonAgentId()).playDefaultRole();
    }

    @Override
    protected void main() throws Exception {
        EnhancedSyncACC ops = getACC();
        TucsonTupleCentreId board = TucsonTupleCentreId.of("board","localhost", "20504");
        LogicTuple stopAll = LogicTuple.parse(
                "[stop(provider1),stop(provider2),stop(provider3),stop(requestor1),stop(requestor2),stop(requestor3)]"
        );
        TucsonOperation stopper = ops.outAll(board, stopAll, null);
        if (stopper.isResultSuccess()) {
            say("Stop sent to providers and requestors");
        } else {
            say("Failed to send stop to providers and requestors");
        }

    }
}

