package it.unibo.sa.respect.loadb;

import alice.tuple.logic.LogicTuple;
import alice.tuplecentre.tucson.api.*;
import alice.tuplecentre.tucson.api.acc.EnhancedSyncACC;
import alice.tuplecentre.tucson.api.acc.NegotiationACC;
import alice.tuplecentre.tucson.api.exceptions.TucsonInvalidAgentIdException;
import alice.tuplecentre.tucson.api.exceptions.TucsonInvalidTupleCentreIdException;

/**
 * Dummy Service Requestor class to show some 'adaptive' features related to
 * usage of uniform primitives. It probabilistically looks for available
 * services then issue a request to the Service Provider found.
 *
 * @author s.mariani@unibo.it
 */
public class ServiceRequestor extends AbstractTucsonAgent<EnhancedSyncACC> {

    /**
     * @param args no args expected.
     */
    public static void main(final String[] args) throws Exception {
        new ServiceRequestor("requestor1", "board@localhost:20504").go();
        new ServiceRequestor("requestor2", "board@localhost:20504").go();
        new ServiceRequestor("requestor3", "board@localhost:20504").go();
    }

    private static final Long TIMEOUT = null; // no timeout

    private final TucsonTupleCentreId tid;

    /**
     * @param aid  agent name
     * @param node node where to look for services
     * @throws TucsonInvalidAgentIdException if the chosen ID is not a valid TuCSoN agent ID
     */
    public ServiceRequestor(final String aid, final String node) throws TucsonInvalidAgentIdException, TucsonInvalidTupleCentreIdException {
        super(aid);
        tid = TucsonTupleCentreId.of(node);
    }

    @Override
    protected EnhancedSyncACC retrieveACC(TucsonAgentId tucsonAgentId, String s, int i) throws Exception {
        return TucsonMetaACC.getNegotiationContext(getTucsonAgentId()).playDefaultRole();
    }

    @Override
    protected void main() throws Exception {
        EnhancedSyncACC acc = getACC();

        TucsonOperation op;
        LogicTuple template;
        LogicTuple service;
        LogicTuple request;
        final LogicTuple dieTuple = LogicTuple.parse("stop(" + getTucsonAgentId().getLocalName() + ")");
        while (true) {
            say("Checking termination...");
            op = acc.inp(tid, dieTuple, TIMEOUT);
            if (op.isResultSuccess()) {
                break;
            }
            /*
             * Service search phase.
             */
            template = LogicTuple.parse("ad(S)");
            say("Looking for services...");
            /*
             * Experiment alternative primitives and analyse different behaviours.
             */
            op = acc.rd(tid, template, TIMEOUT);
//            op = acc.urd(tid, template, TIMEOUT);

            service = op.getLogicTupleResult();

            /*
             * Request submission phase.
             */
            say("Submitting request for service: %s", service.getArg(0));
            request = LogicTuple.parse("req(" + service.getArg(0) + ")");
            acc.out(tid, request, TIMEOUT);
            Thread.sleep(1000);
        }
        say("Someone killed me, bye!");
    }

}