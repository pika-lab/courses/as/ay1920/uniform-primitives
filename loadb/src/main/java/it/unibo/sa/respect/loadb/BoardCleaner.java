package it.unibo.sa.respect.loadb;

import alice.tuple.logic.LogicTuple;
import alice.tuplecentre.tucson.api.*;
import alice.tuplecentre.tucson.api.acc.EnhancedSyncACC;
import alice.tuplecentre.tucson.api.exceptions.TucsonInvalidAgentIdException;

public class BoardCleaner extends AbstractTucsonAgent<EnhancedSyncACC> {

    public static void main(final String[] args) {
        try {
            new BoardCleaner("cleaner").go();
        } catch (final TucsonInvalidAgentIdException e) {
            e.printStackTrace();
        }
    }

    public BoardCleaner(final String aid) throws TucsonInvalidAgentIdException {
        super(aid);
    }

    @Override
    protected EnhancedSyncACC retrieveACC(TucsonAgentId tucsonAgentId, String s, int i) throws Exception {
        return TucsonMetaACC.getNegotiationContext(getTucsonAgentId()).playDefaultRole();
    }

    @Override
    protected void main() throws Exception {
        EnhancedSyncACC ops = getACC();
        TucsonTupleCentreId board = TucsonTupleCentreId.of("board","localhost", "20504");
        TucsonOperation cleanAds = ops.inAll(board, LogicTuple.parse("ad(_)"), null);
        TucsonOperation cleanStops = ops.inAll(board, LogicTuple.parse("stop(_)"), null);
        if (cleanAds.isResultSuccess() & cleanStops.isResultSuccess()) {
            say("Succesfully cleaned service board");
        } else {
            say("Failed to clean service board");
        }
    }
}

