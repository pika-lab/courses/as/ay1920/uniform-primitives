task<JavaExec>("runClients") {
    dependsOn("classes")
    sourceSets {
        main {
            classpath = runtimeClasspath
        }
    }
    main = "it.unibo.sa.respect.loadb.ServiceRequestor"
    standardInput = System.`in`

}

task<JavaExec>("runServers") {
    dependsOn("classes")
    sourceSets {
        main {
            classpath = runtimeClasspath
        }
    }
    main = "it.unibo.sa.respect.loadb.ServiceProvider"
    standardInput = System.`in`
}

task<JavaExec>("stopCSAll") {
    sourceSets {
        main {
            classpath = runtimeClasspath
        }
    }
    main = "it.unibo.sa.respect.loadb.Stopper"
    standardInput = System.`in`
}

task<JavaExec>("cleanBoard") {
    sourceSets {
        main {
            classpath = runtimeClasspath
        }
    }
    main = "it.unibo.sa.respect.loadb.BoardCleaner"
    standardInput = System.`in`
}

