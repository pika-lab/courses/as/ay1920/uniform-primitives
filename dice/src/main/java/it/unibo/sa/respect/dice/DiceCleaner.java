package it.unibo.sa.respect.dice;

import alice.tuple.logic.LogicTuple;
import alice.tuple.logic.TupleArgument;
import alice.tuplecentre.tucson.api.AbstractTucsonAgent;
import alice.tuplecentre.tucson.api.TucsonAgentId;
import alice.tuplecentre.tucson.api.TucsonMetaACC;
import alice.tuplecentre.tucson.api.TucsonTupleCentreId;
import alice.tuplecentre.tucson.api.acc.EnhancedSyncACC;
import alice.tuplecentre.tucson.api.exceptions.TucsonInvalidAgentIdException;
import alice.tuplecentre.tucson.api.exceptions.TucsonInvalidTupleCentreIdException;

public class DiceCleaner extends AbstractTucsonAgent<EnhancedSyncACC> {

    public static void main(final String[] args) throws TucsonInvalidAgentIdException, TucsonInvalidTupleCentreIdException {
        new DiceCleaner("cleaner") {
            @Override
            protected void onTermination() {
                System.exit(0);
            }
        }.go();
    }

    private final TucsonTupleCentreId tcid;

    public DiceCleaner(String aid) throws TucsonInvalidAgentIdException, TucsonInvalidTupleCentreIdException {
        super(aid);
        tcid = TucsonTupleCentreId.of("dice", "localhost", "20504");
    }

    @Override
    protected EnhancedSyncACC retrieveACC(TucsonAgentId tucsonAgentId, String s, int i) throws Exception {
        return TucsonMetaACC.getNegotiationContext(getTucsonAgentId()).playDefaultRole();
    }

    @Override
    protected void main() throws Exception {
        LogicTuple template = LogicTuple.of("face", TupleArgument.var());

        getACC().inAll(tcid, template, null);
    }

}
