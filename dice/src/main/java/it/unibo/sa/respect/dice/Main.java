package it.unibo.sa.respect.dice;

import alice.tuple.logic.LogicTuple;
import alice.tuple.logic.exceptions.InvalidLogicTupleException;
import alice.tuplecentre.api.exceptions.OperationTimeOutException;
import alice.tuplecentre.tucson.api.TucsonAgentId;
import alice.tuplecentre.tucson.api.TucsonMetaACC;
import alice.tuplecentre.tucson.api.TucsonTupleCentreId;
import alice.tuplecentre.tucson.api.acc.EnhancedSyncACC;
import alice.tuplecentre.tucson.api.acc.NegotiationACC;
import alice.tuplecentre.tucson.api.exceptions.TucsonInvalidAgentIdException;
import alice.tuplecentre.tucson.api.exceptions.TucsonInvalidTupleCentreIdException;
import alice.tuplecentre.tucson.api.exceptions.TucsonOperationNotPossibleException;
import alice.tuplecentre.tucson.api.exceptions.UnreachableNodeException;
import alice.tuplecentre.tucson.network.exceptions.DialogInitializationException;
import alice.tuplecentre.tucson.service.TucsonNodeService;

public class Main {

    /**
     * @param args no args expected.
     */
    public static void main(final String[] args) throws DialogInitializationException, TucsonInvalidAgentIdException, UnreachableNodeException, OperationTimeOutException, TucsonOperationNotPossibleException, TucsonInvalidTupleCentreIdException, InvalidLogicTupleException {
        TucsonNodeService node = new TucsonNodeService();
        node.install();

        while (!TucsonNodeService.isInstalled(1000)) {
            System.out.println("Waiting for TuCSoN Node to boot...");
        }

        System.out.println("...boot done, now configuring space...");
        final NegotiationACC negAcc = TucsonMetaACC.getNegotiationContext(TucsonAgentId.of("god"));
        final EnhancedSyncACC acc = negAcc.playDefaultRole();
        acc.outAll(
                TucsonTupleCentreId.of("dice", "localhost", "20504"),
                LogicTuple.parse("[face(1),face(2),face(3),face(4),face(5),face(6)]"),
                Long.MAX_VALUE
        );
        System.out.println("...configuration done, now starting agent...");
        new DicePlayer("roller") {
            @Override
            protected void onTermination() {
                System.exit(0);
            }
        }.go();
    }
}
