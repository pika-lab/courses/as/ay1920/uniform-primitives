package it.unibo.sa.respect.dice;

import alice.tuple.logic.LogicTuple;
import alice.tuplecentre.tucson.api.*;
import alice.tuplecentre.tucson.api.acc.EnhancedSyncACC;
import alice.tuplecentre.tucson.api.exceptions.TucsonInvalidAgentIdException;
import alice.tuplecentre.tucson.api.exceptions.TucsonInvalidTupleCentreIdException;

public class DiceBuilder extends AbstractTucsonAgent<EnhancedSyncACC> {

    public static void main(final String[] args) throws TucsonInvalidAgentIdException, TucsonInvalidTupleCentreIdException {
        new DiceBuilder("builder") {
            @Override
            protected void onTermination() {
                System.exit(0);
            }
        }.go();
    }

    private final TucsonTupleCentreId tcid;

    public DiceBuilder(String aid) throws TucsonInvalidAgentIdException, TucsonInvalidTupleCentreIdException {
        super(aid);
        tcid = TucsonTupleCentreId.of("dice", "localhost", "20504");
    }

    @Override
    protected EnhancedSyncACC retrieveACC(TucsonAgentId tucsonAgentId, String s, int i) throws Exception {
        return TucsonMetaACC.getNegotiationContext(getTucsonAgentId()).playDefaultRole();
    }

    @Override
    protected void main() throws Exception {
        TucsonOperation op = getACC().outAll(tcid,
                LogicTuple.parse("[face(1),face(2),face(3),face(4),face(5),face(6)]"),
                Long.MAX_VALUE);

        if (op.isResultSuccess()) {
            say("Dice configured");
        } else {
            say("WHAAAT?");
        }
    }
}
