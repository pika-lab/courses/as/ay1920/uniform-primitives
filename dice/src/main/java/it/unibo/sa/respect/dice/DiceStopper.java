package it.unibo.sa.respect.dice;

import alice.tuple.logic.LogicTuple;
import alice.tuple.logic.TupleArgument;
import alice.tuplecentre.tucson.api.*;
import alice.tuplecentre.tucson.api.acc.EnhancedSyncACC;
import alice.tuplecentre.tucson.api.exceptions.TucsonInvalidAgentIdException;
import alice.tuplecentre.tucson.api.exceptions.TucsonInvalidTupleCentreIdException;

public class DiceStopper extends AbstractTucsonAgent<EnhancedSyncACC> {

  public static void main(final String[] args) throws TucsonInvalidAgentIdException, TucsonInvalidTupleCentreIdException {
      new DiceStopper("stopper") {
          @Override
          protected void onTermination() {
              System.exit(0);
          }
      }.go();
  }

    private final TucsonTupleCentreId tcid;

    public DiceStopper(String aid) throws TucsonInvalidAgentIdException, TucsonInvalidTupleCentreIdException {
        super(aid);
        tcid = TucsonTupleCentreId.of("dice", "localhost", "20504");
    }

    @Override
    protected EnhancedSyncACC retrieveACC(TucsonAgentId tucsonAgentId, String s, int i) throws Exception {
        return TucsonMetaACC.getNegotiationContext(getTucsonAgentId()).playDefaultRole();
    }

    @Override
    protected void main() throws Exception {

        EnhancedSyncACC acc = getACC();

        LogicTuple dieTuple = LogicTuple.of("stop", TupleArgument.of("roller"));

        TucsonOperation op = acc.out(tcid, dieTuple, null);
        if (op.isResultSuccess()) {
            say("Roller stopped");
        } else {
            say("WHAAAT?");
        }

    }
}
