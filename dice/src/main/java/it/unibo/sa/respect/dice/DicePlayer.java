package it.unibo.sa.respect.dice;

import alice.tuple.logic.LogicTuple;
import alice.tuple.logic.TupleArgument;
import alice.tuple.logic.exceptions.InvalidLogicTupleException;
import alice.tuplecentre.api.exceptions.OperationTimeOutException;
import alice.tuplecentre.tucson.api.*;
import alice.tuplecentre.tucson.api.acc.EnhancedSyncACC;
import alice.tuplecentre.tucson.api.acc.NegotiationACC;
import alice.tuplecentre.tucson.api.exceptions.TucsonInvalidAgentIdException;
import alice.tuplecentre.tucson.api.exceptions.TucsonInvalidTupleCentreIdException;
import alice.tuplecentre.tucson.api.exceptions.TucsonOperationNotPossibleException;
import alice.tuplecentre.tucson.api.exceptions.UnreachableNodeException;

import java.util.HashMap;
import java.util.Map;


public class DicePlayer extends AbstractTucsonAgent<EnhancedSyncACC> {

    public static void main(String[] args) throws TucsonInvalidAgentIdException, TucsonInvalidTupleCentreIdException {
        new DicePlayer("roller") {
            @Override
            protected void onTermination() {
                System.exit(0);
            }
        }.go();
    }

    private static final Long TIMEOUT = null; // no timeout

    private final TucsonTupleCentreId tcid;
    private final Map<Integer, Integer> outcomes;

    public DicePlayer(String aid) throws TucsonInvalidAgentIdException, TucsonInvalidTupleCentreIdException {
        super(aid);
        tcid = TucsonTupleCentreId.of("dice", "localhost", "20504");
        outcomes = new HashMap<>();
    }

    @Override
    protected EnhancedSyncACC retrieveACC(TucsonAgentId tucsonAgentId, String s, int i) throws Exception {
        return TucsonMetaACC.getNegotiationContext(getTucsonAgentId()).playDefaultRole();
    }

    @Override
    protected void main() throws Exception {
        EnhancedSyncACC acc = getACC();
        TucsonOperation op;
        LogicTuple template;
        LogicTuple dieTuple = LogicTuple.of("stop", TupleArgument.of(getTucsonAgentId().getLocalName()));
        int face;
        Integer nTimes = 1;

        while (true) {
            say("Checking termination...");

            op = acc.inp(tcid, dieTuple, TIMEOUT);
            if (op.isResultSuccess()) {
                break;
            }

            template = LogicTuple.of("face", TupleArgument.var());

            say("Rolling dice...");
            op = acc.rd(tcid, template, TIMEOUT);
//            say("Rolling dice uniformly...");
//            op = acc.urd(tcid, template, TIMEOUT);

            if (op.isResultSuccess()) {
                face = op.getLogicTupleResult().getArg(0).intValue();
                say("...they see me rollin', they hatin': " + face);
                nTimes = outcomes.get(face);
                if (nTimes == null) {
                    outcomes.put(face, 1);
                } else {
                    outcomes.put(face, nTimes + 1);
                }
            }
            printStats();
            Thread.sleep(500);
        }

        say("Someone killed me, bye!");
        printStats(true);
    }

    private void printStats() {
        printStats(false);
    }

    private void printStats(boolean end) {
        if (end) {
            say("Final outcomes 'till now:");
        } else {
            say("Outcomes 'till now:");
        }
        Integer sum = outcomes.values().stream().mapToInt(i -> i).sum();
        for (Integer i : outcomes.keySet()) {
            Integer t = outcomes.get(i);
            say("\tFace %d drawn %d of %d times (ratio: %.2f%%)", i, t, sum, ratio(t, sum));
        }
    }

    private double ratio(Number x, Number y) {
        return x.doubleValue() * 100d / y.doubleValue();
    }

}
