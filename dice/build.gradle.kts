task<JavaExec>("makeDice") {
    sourceSets {
        main {
            classpath = runtimeClasspath
        }
    }
    main = "it.unibo.sa.respect.dice.DiceBuilder"
    standardInput = System.`in`
}

task<JavaExec>("cleanDice") {
    sourceSets {
        main {
            classpath = runtimeClasspath
        }
    }
    main = "it.unibo.sa.respect.dice.DiceCleaner"
    standardInput = System.`in`
}

task<JavaExec>("rollDice") {
    sourceSets {
        main {
            classpath = runtimeClasspath
        }
    }
    main = "it.unibo.sa.respect.dice.DicePlayer"
}

task<JavaExec>("stopDice") {
    sourceSets {
        main {
            classpath = runtimeClasspath
        }
    }
    main = "it.unibo.sa.respect.dice.DiceStopper"
}

task<JavaExec>("runDice") {
    sourceSets {
        main {
            classpath = runtimeClasspath
        }
    }
    main = "it.unibo.sa.respect.dice.Main"
    standardInput = System.`in`
}