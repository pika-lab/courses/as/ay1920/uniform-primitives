# TuCSoN uniform primitives exercises

### Important remarks

* You can start a TuCSoN **node** by running:

    ```bash
    ./gradlew tucson
    ```
  
    this will start a service listening on port 20504.
    
    * You can specify another port by starting the node as follows:
        
        ```bash
        ./gradlew tucson -Pport="port here"
        ```

* Always remember to run:

    ```bash
    ./gradlew --stop
    ```

    after each demo is completed, in order to ensure all TuCSoN nodes are actually shutdown.

* You can start a TuCSoN **inspector** by running:

    ```bash
    ./gradlew inspector
    ```
  
    A GUI application will start and you will be able to select the actual node and tuple centre to inspect

* You can always start a TuCSoN CLI (Command Line Interface) by running:

    ```bash
    ./gradlew cli -Pport=<node port>
    ```
  
    The `-Pport=<node port>` part is optional, and the `port` property defaults to `20504`.

## Exercise 1 -- Rolling Dices (`dice` module)

Just run:

* Start a TuCSoN node

    ```bash
    ./gradlew tucson
    ```
* Start a TuCSoN Inspector

  ```bash
  ./gradlew inspector
  ```

  and make it observe tuples in tuple space `dice`

* Configure the tuple space `dice`

  ```bash
  ./gradlew makeDice
  ```

  and observe its state in the Inspector

* Pick your getter primitive (`rd`/`urd`), run the roller

  ```bash
  ./gradlew rollDice
  ```

  and observe its output

* Stop the roller with

  ```bash
  ./gradlew stopDice
  ```
  and observe its final output

* Repeat the experiment with the other implementations of the roller (`rd`/`urd`)

* Clean up the tuple space `dice` with

  ```bash
  ./gradlew cleanDice
  ```

* Switch off both the Inspector and TuCSoN

## Exercise 2 -- Load Balancing (`loadb` module)

* Start a TuCSoN node

  ```bash
  ./gradlew tucson
  ```

* Start a TuCSoN Inspector

  ```bash
  ./gradlew inspector
  ```

  and make it observe tuples in tuple space `board`

* Switch on servers

  ```bash
  ./gradlew runServers
  ```

  and observe tuples in tuple space `board`

* Pick your getter primitive (`rd`/`urd`) on the clients, and switch them on

  ```bash
  ./gradlew runClients
  ```

  and keep observing tuples in tuple space `board`

* Stop both clients and servers with

  ```bash
  ./gradlew stopCSAll
  ```

  and observe the output

* Repeat the experiment with the other implementations of the client (`rd`/`urd`)

* Clean up the tuple space `board` with

  ```bash
  ./gradlew cleanBoard
  ```

* Switch off both the Inspector and TuCSoN


## Exercise 3 -- Swarm Intelligence (`swarm` module)

### Fast track

* Run ant swarm

  ```bash
  ./gradlew runSwarms
  ```

### Step-by-step

* Generate topology

  ```bash
  ./gradlew buildAntsTopology
  ```

  and click "Update View" to see the ants

* Configure environment

  ```bash
  ./gradlew buildAntsEnvironment
  ```

* Start swarms with GUI

  ```bash
  ./gradlew runGUI
  ```

  and click "Update View" to see the ants
