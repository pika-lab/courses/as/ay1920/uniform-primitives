plugins {
    `java-library`
}

allprojects {
    repositories {
        jcenter()
        maven("https://dl.bintray.com/pika-lab/tucson")
    }
}

val tucsonVersion: String by project

val tucsonService by configurations.creating
val tucsonCli by configurations.creating
val tucsonInspector by configurations.creating

dependencies {

    tucsonService("it.unibo.tucson", "service", tucsonVersion)
    tucsonCli("it.unibo.tucson", "cli", tucsonVersion)
    tucsonInspector("it.unibo.tucson", "inspector", tucsonVersion)

}

subprojects {

    apply(plugin = "java-library")

    dependencies {
        api("it.unibo.tucson", "core", tucsonVersion)
        api("it.unibo.tucson", "service", tucsonVersion)
        api("it.unibo.tucson", "client", tucsonVersion)
        implementation("org.slf4j", "slf4j-nop", "1.7.30")

        testImplementation("junit:junit:4.12")
    }
}

tasks.create<JavaExec>("tucson") {
    group = "tucson"
    classpath = tucsonService
    main = "alice.tuplecentre.tucson.service.TucsonNodeService"
    standardInput = System.`in`

    if (properties.containsKey("port")) {
        args = listOf("-portno", properties["port"].toString())
    }
}

tasks.create<JavaExec>("cli") {
    group = "tucson"
    classpath = tucsonCli
    main = "alice.tuplecentre.tucson.service.tools.CommandLineInterpreter"
    standardInput = System.`in`

    if (properties.containsKey("port")) {
        args = listOf("-portno", properties["port"].toString())
    }
}

tasks.create<JavaExec>("inspector") {
    group = "tucson"
    classpath = tucsonInspector
    main = "alice.tuplecentre.tucson.introspection.tools.InspectorGUI"
    standardInput = System.`in`
}